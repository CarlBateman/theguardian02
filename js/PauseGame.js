"use strict";
function makePauseGame(gameObj, xrHelper, camera) {
  const canvas = document.getElementById('renderCanvas');

  let scene = xrHelper.baseExperience.scene;

  let currentCamera = camera;

  currentCamera.position.set(0, 20, -30);
  currentCamera.setTarget(BABYLON.Vector3.Zero());

  let cameraRot = currentCamera.rotationQuaternion.clone();
  let cameraPos = currentCamera.position.clone();

  let crosshair = BABYLON.MeshBuilder.CreatePlane("crosshair", { width: .16, size: .16, tileSize: 1 }, scene);
  crosshair.material = new BABYLON.StandardMaterial("myMaterial", scene);
  crosshair.material.diffuseTexture = new BABYLON.Texture("images/cursor.png", scene, false, true, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
  crosshair.material.diffuseTexture.hasAlpha = true;
  crosshair.material.emissiveTexture = new BABYLON.Texture("images/cursor.png", scene, false, true, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
  crosshair.position.z = 6;
  crosshair.isPickable = false;
  crosshair.parent = currentCamera;
  crosshair.renderingGroupId = 3;

  const root = scene.getTransformNodesByID("root")[0];

  let plane = createTextThing(scene, "plane", " Click anywhere to start ", "Click the sky to pause");
  //let plane = createTextThing(scene, "plane", " 18 anywhere to start ", "Click the sky to pause");
  plane.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

  gameObj.name = "pause";

  gameObj.handlePointerEvent = function handlePointerEvent(pointerInfo) {
    switch (pointerInfo.type) {
      //case BABYLON.PointerEventTypes.POINTERMOVE:
      //  break;
      case BABYLON.PointerEventTypes.POINTERUP:
      case "selectend":
        pointerInfo.type
        gameObj.next = "play";
        break;
    }
  };

  gameObj.handleOnXRSessionInit = function handleOnXRSessionInit() {
    cameraRot = camera.rotationQuaternion.clone();
    cameraPos = camera.position.clone();
    xrHelper.baseExperience.sessionManager.onXRFrameObservable.addOnce(() => {
      xrHelper.baseExperience.camera.rotationQuaternion.copyFrom(cameraRot);
      xrHelper.baseExperience.camera.position.copyFrom(cameraPos);
    });

    currentCamera = xrHelper.baseExperience.camera;
    crosshair.parent = currentCamera;
  };

  gameObj.handleOnXRSessionEnded = function handleOnXRSessionEnded() {
    cameraRot = xrHelper.baseExperience.camera.rotationQuaternion.clone();
    cameraPos = xrHelper.baseExperience.camera.position.clone();

    currentCamera = camera;

    scene.onBeforeRenderObservable.addOnce(() => {
      currentCamera.rotationQuaternion.copyFrom(cameraRot);
      currentCamera.position.copyFrom(cameraPos);
    });
    currentCamera.rotationQuaternion.copyFrom(cameraRot);
    currentCamera.position.copyFrom(cameraPos);

    crosshair.parent = currentCamera;
  };

  gameObj.stop = function () {
    if (xrHelper.baseExperience.state !== BABYLON.WebXRState.IN_XR) {
      currentCamera.detachControl(canvas);
    }

    cameraRot = currentCamera.rotationQuaternion.clone();
    cameraPos = currentCamera.position.clone();

    plane.setEnabled(false);

    crosshair.visibility = false;
  };

  gameObj.start = function () {
    gameObj.next = null;

    if (xrHelper.baseExperience.state === BABYLON.WebXRState.IN_XR) {
      currentCamera = xrHelper.baseExperience.camera;
    } else {
      currentCamera = camera;
    }
    currentCamera.rotationQuaternion.copyFrom(cameraRot);
    currentCamera.position.copyFrom(cameraPos);

    if (xrHelper.baseExperience.state !== BABYLON.WebXRState.IN_XR) {
      currentCamera.attachControl(canvas, true);
    }

    plane.setEnabled(true);

    crosshair.parent = currentCamera;
    crosshair.visibility = true;
  };

  gameObj.update = function (dt) {
    root.rotation.y += dt * Math.PI / 360;
  }

  return gameObj;
}