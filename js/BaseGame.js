// this is the default
// does nothing
"use strict";
function makeBaseGame() {
  //let obj = {};
  //obj.handleKeyEvent = function handleKeyEvent() { };
  //obj.handlePointerEvent = function handlePointerEvent() { };

  return {
    next: null,
    handleKeyEvent: function handleKeyEvent(kbInfo) { },
    handlePointerEvent: function handlePointerEvent() { },
    handleOnXRSessionInit: function handleOnXRSessionInit() {
      //console.log("handleOnXRSessionInit");
    },
    handleOnXRSessionEnded: function handleOnXRSessionEnded() {
      //console.log("handleOnXRSessionEnded");
    },

    start: function () { },
    stop: function () { },
    update: function () { },
  };
}