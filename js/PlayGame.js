"use strict";
function makePlayGame(gameObj, xrHelper, camera) {
  const layer = { crosshair: 3, pause: 1, confirm: 2 }
  const pause = { none: 0, pending: 2, clickedOnPause: 1, waitingToConfirm: 4 };
  const canvas = document.getElementById('renderCanvas');

  let scene = xrHelper.baseExperience.scene;
  let pointerState = pause.none;

  let currentCamera = camera;
  currentCamera.position.set(0, 5, -5);

  //currentCamera.rotationQuaternion = currentCamera.rotation.toQuaternion();
  currentCamera.setTarget(BABYLON.Vector3.Zero());

  let cameraRot = currentCamera.rotationQuaternion.clone();
  let cameraPos = currentCamera.position.clone();

  let crosshair = BABYLON.MeshBuilder.CreatePlane("crosshair", { width: .16, size: .16, tileSize: 1 }, scene);
  crosshair.material = new BABYLON.StandardMaterial("myMaterial", scene);
  crosshair.material.diffuseTexture = new BABYLON.Texture("images/cursor.png", scene, false, true, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
  crosshair.material.diffuseTexture.hasAlpha = true;
  crosshair.material.emissiveTexture = new BABYLON.Texture("images/cursor.png", scene, false, true, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
  crosshair.position.z = 6;
  crosshair.isPickable = false;
  crosshair.parent = currentCamera;
  crosshair.renderingGroupId = layer.crosshair;
  crosshair.visibility = false;

  let pausePlane = BABYLON.MeshBuilder.CreatePlane("pausePlane", { width: 100, height: 100, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
  pausePlane.rotation.x = Math.PI / 2;
  pausePlane.position.x = currentCamera.position.x;
  pausePlane.position.y = currentCamera.position.y + 5;
  pausePlane.position.z = currentCamera.position.z;
  pausePlane.visibility = false;
  pausePlane.renderingGroupId = layer.pause;
  pausePlane.setEnabled(false);

  let planeConfirm = createTextThing(scene, "confirm", "      Click to confirm     ", "pause");
  planeConfirm.setEnabled(false);
  planeConfirm.renderingGroupId = layer.confirm;
  planeConfirm.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

  gameObj.name = "play";

  
  function predicate(layer) {
    return function a(mesh) {
      return mesh.renderingGroupId == layer && mesh.isVisible;
    }
  }

  // click down and up on plane to get confirmation
  // click up to confirm
  gameObj.handlePointerEvent = function handlePointerEvent(pointerInfo) {
    switch (pointerInfo.type) {
      case BABYLON.PointerEventTypes.POINTERMOVE:
        break;
      case BABYLON.PointerEventTypes.POINTERDOWN:
      case "selectstart":
        if (pointerState === pause.none) {
          var pickResult = castRay(currentCamera, scene, crosshair, predicate(layer.pause));

          if (pickResult.pickedMesh && pickResult.pickedMesh.id == "pausePlane") {
            pointerState = pause.clickedOnPause;
          }
        }

        break;

      case BABYLON.PointerEventTypes.POINTERUP:
      case "selectend":
        if (pointerState === pause.waitingToConfirm) {
          var pickResult = castRay(currentCamera, scene, crosshair, predicate(layer.confirm));

          if (pickResult.pickedMesh && pickResult.pickedMesh.id === "confirm") {
            planeConfirm.setEnabled(false);
            pointerState = pause.none;
            gameObj.next = "pause";
          } else {
            planeConfirm.setEnabled(false);
            pointerState = pause.none;
          }
        }

        if (pointerState === pause.clickedOnPause) {
          var pickResult = castRay(currentCamera, scene, crosshair, predicate(layer.pause));

          if (pickResult.pickedMesh && pickResult.pickedMesh.id == "pausePlane") {
            let fr = currentCamera.getForwardRay();
            let pos = currentCamera.position.add(fr.direction.scale(50));

            planeConfirm.position.copyFrom(pos);
            planeConfirm.setEnabled(true);
            pointerState = pause.waitingToConfirm
          } else {
            planeConfirm.setEnabled(false);
            pointerState = pause.none;
          }
        }

        break;
    }
  };
  gameObj.handleOnXRSessionInit = function handleOnXRSessionInit() {
    cameraRot = camera.rotationQuaternion.clone();
    cameraPos = camera.position.clone();

    xrHelper.baseExperience.sessionManager.onXRFrameObservable.addOnce(() => {
      xrHelper.baseExperience.camera.rotationQuaternion.copyFrom(cameraRot);
      xrHelper.baseExperience.camera.position.copyFrom(cameraPos);
    });

    currentCamera = xrHelper.baseExperience.camera;
    crosshair.parent = currentCamera;
  };

  gameObj.handleOnXRSessionEnded = function handleOnXRSessionEnded() {
    cameraRot = xrHelper.baseExperience.camera.rotationQuaternion.clone();
    cameraPos = xrHelper.baseExperience.camera.position.clone();
    currentCamera = camera;
    scene.onBeforeRenderObservable.addOnce(() => {
      currentCamera.rotationQuaternion.copyFrom(cameraRot);
      currentCamera.position.copyFrom(cameraPos);
    });
    currentCamera.rotationQuaternion.copyFrom(cameraRot);
    currentCamera.position.copyFrom(cameraPos);

    crosshair.parent = currentCamera;
  };

  gameObj.stop = function () {
    if (xrHelper.baseExperience.state !== BABYLON.WebXRState.IN_XR) {
      currentCamera.detachControl(canvas);
    }

    cameraRot = currentCamera.rotationQuaternion.clone();
    cameraPos = currentCamera.position.clone();

    pausePlane.setEnabled(false);
    planeConfirm.setEnabled(false);

    crosshair.visibility = false;
  };

  gameObj.start = function () {
    gameObj.next = null;

    if (xrHelper.baseExperience.state === BABYLON.WebXRState.IN_XR) {
      currentCamera = xrHelper.baseExperience.camera;
    } else {
      currentCamera = camera;
    }
    currentCamera.rotationQuaternion.copyFrom(cameraRot);
    currentCamera.position.copyFrom(cameraPos);

    if (xrHelper.baseExperience.state !== BABYLON.WebXRState.IN_XR) {
      currentCamera.attachControl(canvas, true);
    }

    pausePlane.visibility = true;
    pausePlane.setEnabled(true);

    crosshair.parent = currentCamera;
    crosshair.visibility = true;
  };

  gameObj.update = function () {
    if (xrHelper.baseExperience.state === BABYLON.WebXRState.IN_XR) {
      pausePlane.position.x = xrHelper.baseExperience.camera.position.x;
      pausePlane.position.y = xrHelper.baseExperience.camera.position.y + 2;
      pausePlane.position.z = xrHelper.baseExperience.camera.position.z;
    } else {
      pausePlane.position.x = currentCamera.position.x;
      pausePlane.position.y = currentCamera.position.y + 2;
      pausePlane.position.z = currentCamera.position.z;
    }
  };

  return gameObj;
}