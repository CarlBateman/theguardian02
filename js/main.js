﻿window.addEventListener('DOMContentLoaded', function () {
  "use strict";

  const canvas = document.getElementById('renderCanvas');
  const engine = new BABYLON.Engine(canvas, true);
  let scene;
  let playGame;
  let pauseGame;
  let game;
  let camera;
  let states = [];

  const createScene = async function () {
    const scene = new BABYLON.Scene(engine);
    scene.name = "The Guardian";

    camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 0, 0), scene);
    camera.rotationQuaternion = camera.rotation.toQuaternion();
    camera.attachControl(canvas, true);

    canvas.onclick = function () {
      canvas.requestPointerLock();
    };

    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.7;

    const env = scene.createDefaultEnvironment();
    const xrHelper = await scene.createDefaultXRExperienceAsync();
    xrHelper.pointerSelection = xrHelper.baseExperience.featuresManager.enableFeature(BABYLON.WebXRControllerPointerSelection, 'latest', {
      xrInput: xrHelper.input,
      disableteleportation: true
    });

    createTerrain(scene, 1);

    scene.onKeyboardObservable.add((kbInfo) => {
      game.handleKeyEvent(kbInfo);
    });

    scene.onPointerObservable.add((pointerInfo) => {
      game.handlePointerEvent(pointerInfo);
    });

    scene.onBeforeRenderObservable.add(() => {
      let dt = scene.getEngine().getDeltaTime() / 1000;
      game.update(dt);

      if (game.next !== null) {
        let next = game.next;
        game.stop();
        game = states[next];
        game.start();
      }
    });

    xrHelper.baseExperience.sessionManager.onXRSessionInit.add(() => {
      camera.detachControl(canvas);
      xrHelper.baseExperience.sessionManager.session.onselectstart = onSelectionStartEvent;
      xrHelper.baseExperience.sessionManager.session.onselectend = onSelectionEndEvent;

      function onSelectionStartEvent(event) {
        game.handlePointerEvent(event);
      };
      function onSelectionEndEvent(event) {
        game.handlePointerEvent(event);
      };

      game.handleOnXRSessionInit();
    })
    xrHelper.baseExperience.sessionManager.onXRSessionEnded.add(() => {
      camera.attachControl(canvas, true);

      game.handleOnXRSessionEnded();
    })
    return xrHelper;
  };

  createScene().then(function (xrHelper) {
    scene = xrHelper.baseExperience.scene;
    playGame = makePlayGame(makeBaseGame(), xrHelper, camera );
    pauseGame = makePauseGame(makeBaseGame(), xrHelper, camera);

    states["play"] = playGame;
    states["pause"] = pauseGame;

    game = pauseGame;
    game.start();

    document.getElementById("myModal").hidden = true;
    engine.runRenderLoop(function () {
      scene.render();
    });
  });

  window.addEventListener('resize', function () {
    engine.resize();
  });
});