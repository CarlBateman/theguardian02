﻿function createTextThing(scene, name, text1, text2) {
  //Set font type
  var font_type = "Arial";

  //Set width an height for plane
  var planeWidth = 10;
  var planeHeight = 3;

  //Create plane
  var plane = BABYLON.MeshBuilder.CreatePlane(name, { width: planeWidth, height: planeHeight }, scene);
  plane.position.y = 5;
  //plane.renderingGroupId = 1;

  //Set width and height for dynamic texture using same multiplier
  var DTWidth = planeWidth * 60;
  var DTHeight = planeHeight * 60;

  //Create dynamic texture
  var dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", { width: DTWidth, height: DTHeight }, scene);

  //Check width of text for given font type at any size of font
  var ctx = dynamicTexture.getContext();
  var size = 12; //any value will work
  ctx.font = size + "px " + font_type;
  var textWidth = ctx.measureText(text1).width;
  var textHeight = ctx.measureText("M").width / 2;

  //Calculate ratio of text width to size of font used
  var ratio = textWidth / size;

  //set font to be actually used to write text on dynamic texture
  var font_size = Math.floor(DTWidth / (ratio * 1)); //size of multiplier (1) can be adjusted, increase for smaller text
  var font = font_size + "px " + font_type;

  //Draw text
  dynamicTexture.drawText(text1, null, textHeight * ratio + 10, font, "#000000", "#ffffff", true, true);
  dynamicTexture.drawText(text2, null, textHeight * ratio * 3, font, "#000000", null, true, true);

  //create material
  var mat = new BABYLON.StandardMaterial("mat", scene);
  mat.diffuseTexture = dynamicTexture;
  //mat.diffuseColor = new BABYLON.Color3(1, 1, 1);
  //mat.emissiveTexture  = dynamicTexture;
  mat.emissiveColor = new BABYLON.Color3(1, 1, 1);

  //apply material
  plane.material = mat;

  return plane;
}

function vecToLocal(vector, mesh) {
  var m = mesh.getWorldMatrix();
  var v = BABYLON.Vector3.TransformCoordinates(vector, m);
  return v;
}

function castRay(camera, scene, cursor, predicate) {
  var origin = camera.position;

  var forward = new BABYLON.Vector3(0, 0, 1);
  forward = vecToLocal(forward, camera);

  var direction = forward.subtract(origin);
  direction = BABYLON.Vector3.Normalize(direction);

  var length = 100;

  var ray = new BABYLON.Ray(origin, direction, length);

  //return scene.pickWithRay(ray, (mesh) => {
  //  return (mesh.name !== "confirm" && mesh.name !== cursor.name);
  //});
  return scene.pickWithRay(ray, predicate);
}
